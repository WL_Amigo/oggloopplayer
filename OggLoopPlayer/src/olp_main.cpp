/*
 * olp_main.cpp
 *
 *  Created on: 2014/02/11
 *      Author: Amigo_MacBook
 */

#include "OpenAL/alure.h"
#include "olp_class.h"
#include <string>
#include <chrono>
#include <thread>

static const std::chrono::milliseconds duration(100);

int main(int argc,char** argv){

	OGVPlayer ogvplayer;

	if(!alureInitDevice(nullptr,nullptr)){
		std::cout << "Failed to open sound device : " << alureGetErrorString() << std::endl;
		return 1;
	}

	try{
		if(argc != 2){
			throw std::string("Usage:") + argv[0] + " oggfile.ogg";
		}

		ogvplayer.open(argv[1]);

		ogvplayer.display_info();

		if(!ogvplayer.playback()){
			throw std::string("Couldn't start to play ogg.");
		}

		while(ogvplayer.update()){
			if(!ogvplayer.is_Playing()){
				if(!ogvplayer.playback()){
					throw std::string("Ogg stopped by error.");
				}
				else
					std::cout << "Ogg stream was interrupted.\n" << std::endl;
			}
			std::this_thread::sleep_for(duration);
		}

		std::cout << "Program reached termination normally.";
		std::cin.get();
	}
	catch(std::string& content_error){
		std::cout << content_error << "\n"
				<< "Press any key...";

		std::cin.get();

	}

	try{
	ogvplayer.release();
	}
	catch(std::string& error){

	}

	alureShutdownDevice();

	return 0;

}



