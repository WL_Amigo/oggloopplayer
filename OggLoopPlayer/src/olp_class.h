/*
 * olp_class.h
 *
 *  Created on: 2014/01/26
 *      Author: Amigo_MacBook
 */

#ifndef OLP_CLASS_H_
#define OLP_CLASS_H_

#include <string>
#include <iostream>

#include <stdint.h>
#include <AL/al.h>
#include <ogg/ogg.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>

static const int BUFFER_SIZE = 4096 * 8;

/**
 *
 * OGVPlayer(OggVorbisPlayer)クラスの定義です。
 *
 * このOGVPlayerクラスはOpenALを通じてOggVorbis音声ファイルを
 * ストリーミング再生し、また、"LOOPLENGTH"タグと"LOOPSTART"タグが
 * 埋め込まれている場合にそのタグの情報を用いたループ再生をする機能を
 * 提供します。
 *
 */

class OGVPlayer {

public:

	/**
	 * OGVPlayerのコンストラクタです。
	 * ファイルオープンは行わず、プライベートメンバ変数の初期化のみを行います。
	 */
	OGVPlayer();
	/**
	 * OGVPlayerのコンストラクタです。
	 * プライベートメンバ変数の初期化とOGGファイルの読み込みを同時に行います。
	 * @param path 開くOGGファイルへのファイルパス
	 */
	OGVPlayer(std::string path);
	/**
	 * OGVPlayerクラスのデストラクタです。
	 * 破棄時に再生中のソースを停止し、各種変数の開放とOGGファイルハンドラの開放を行います。
	 */
	~OGVPlayer();
	/**
	 * OGGファイルを読み込み、必要なメンバ変数をセットします。
	 * @param path 開くOGGファイルへのファイルパス
	 */
	void open(std::string path);
	/**
	 * 読み込んだOGGファイルのハンドルを解放します。
	 */
	void release();
	/**
	 * 読み込んだOGGファイルの情報を標準出力へ出力します。
	 * 先にOGGファイルが読み込まれている必要があります。
	 */
	void display_info();
	/**
	 * OGGファイルのストリーミング再生を開始します。
	 * @return 再生できた、もしくは既に再生されている場合はtrue、再生できなかった場合はfalseを返します。
	 */
	bool playback();
	/**
	 * このクラスのALソースの再生状態を取得します。
	 * @return ソースが再生中(AL_PLAYING)ならばtrueを返します。
	 */
	bool is_Playing();
	/**
	 * 必要に応じてストリームを更新します。
	 * このメソッドは定期的に呼び出す必要があります。
	 * @return バッファがアクティブ(ALソースのキューにデータが2つ分入っている)であればtrueを返します。
	 */
	bool update();

protected:

	/**
	 * ALソースとALバッファの初期化を行います。
	 * 正常にALソースとALバッファが作成できたら、isALExistをtrueに変更します。
	 */
	void initALObject();
	/**
	 * OGGをバッファサイズ分デコードし、bufferにALバッファとして格納し、返却します。
	 * OGG読み込み時にエラーが起きた場合に例外がthrowされます。
	 * @param buffer デコード結果を格納するバッファ(ALuint)
	 * @return bufferにデータが書き込めたらtrueを返します。OGGファイルがEOFに達した場合はfalseが返ります。
	 */
	bool stream(ALuint buffer);
	/**
	 * ALソースのキューを空にします。
	 */
	void empty();
	/**
	 * OpenALのエラーチェックをします。
	 */
	void check();
	/**
	 * OGGファイルの取扱中に発生したエラーの内容を、エラーコードから標準文字列クラスへ変換します。
	 * @param code OVエラーコード
	 * @return エラー内容を記述した標準文字列クラス
	 */
	std::string errorString(int code);

private:

	FILE* oggFile;					/**< OGGファイルハンドラ */
	OggVorbis_File oggStream;		/**< OGGストリームハンドラ */
	vorbis_info* vorbisInfo;		/**< OGGVorbisファイルの固有情報 */
	vorbis_comment* vorbisComment;	/**< OGGVorbisファイルのユーザーコメント */

	ALuint buffers[2];	/**< ダブルバッファリング用の2連バッファ */
	ALuint source;		/**< ALソース */
	/**
	 * ストリームのフォーマットを決定するAL定数値
	 * OGGファイルの内部情報によって、AL_FORMAT_XXYY(X=ステレオ/モノラル、Y=ビット数)が決定され、格納されます。
	 */
	ALenum format;
	bool isALExist;		/**< ALソースとALバッファが存在するかどうかのフラグ */

	bool flagRangeLoop;		/**< 区間ループ再生をするかどうかの判定フラグ */
	int64_t loopStart;	/**< ループ始点(PCM-Sample)*/
	int64_t loopEnd;	/**< ループ終点(PCM-Sample)*/

};

#endif /* OLP_CLASS_H_ */
