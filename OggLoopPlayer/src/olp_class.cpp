/*
 * olp_class.cpp
 *
 *  Created on: 2014/02/06
 *      Author: Amigo_MacBook
 */

#include <stdio.h>
#include "olp_class.h"
#include "OpenAL/alure.h"

OGVPlayer::OGVPlayer() {

	//ポインタは全てnullptrを指定。oggStreamは勝手に初期化出来ないので放置。
	oggFile = nullptr;
	vorbisInfo = nullptr;
	vorbisComment = nullptr;

	buffers[0] = 0;
	buffers[1] = 0;
	source = 0;
	format = 0;

	loopEnd = -1;
	loopStart = -1;

	isALExist = false;

	//デフォルト値をセット
	flagRangeLoop = false;

}

OGVPlayer::OGVPlayer(std::string path) {

	//与えられたパスのOGGファイルをオープン
	open(path);

	isALExist = false;

	//必要なALオブジェクトを生成
	initALObject();
	check();

}

OGVPlayer::~OGVPlayer() {

	//ソースを停止し、バッファを解放する

	//OGGファイルを開放

}

void OGVPlayer::open(std::string path) {

	int result;
	std::string user_comment;

	//OGGファイルをオープン(FILE)
	if (!(oggFile = fopen(path.c_str(), "rb")))
		throw std::string("Couldn't open OGG file.");

	//OGGファイルをオープン(OggVorbis_File)
	if ((result = ov_open(oggFile, &oggStream, nullptr, 0)) < 0) {
		fclose(oggFile);

		throw std::string("Couldn't make OGG stream. Error : ")
				+ errorString(result);
	}

	//ファイル情報とユーザーコメントを取得
	vorbisInfo = ov_info(&oggStream, -1);
	vorbisComment = ov_comment(&oggStream, -1);

	//ファイル情報-チャンネル数を見て、バッファフォーマットを決定
	if (vorbisInfo->channels == 2)
		format = AL_FORMAT_STEREO16;
	else
		format = AL_FORMAT_MONO16;

	//区間ループの設定を検出し、LOOPSTART/LOOPLENGTHタグが存在すれば設定を読み込み、
	//タグが存在しない場合は区間ループをオフにする
	loopStart = -1;
	loopEnd = -1;
	for (int i = 0; i < vorbisComment->comments; i++) {
		user_comment = vorbisComment->user_comments[i];
		if (user_comment.find("LOOPSTART") != std::string::npos) {
			user_comment.erase(0, 10);
			loopStart = std::stoi(user_comment);
		} else if (user_comment.find("LOOPLENGTH") != std::string::npos) {
			user_comment.erase(0, 11);
			//一旦、loopEndにLOOPLENGTHの値を格納
			loopEnd = std::atoi(user_comment.c_str());
		}
	}
	//両方の値が読み込めていたら、loopEnd(=LOOPLENGTH)を正しい値にセット(LOOPLENGTH+LOOPSTART)
	//両方の値が揃っていなければ、区間ループをオフにしておく
	if (loopStart != -1 && loopEnd != -1) {
		loopEnd = loopEnd + loopStart;
		flagRangeLoop = true;
	} else
		flagRangeLoop = false;

}

void OGVPlayer::release() {

	//ALソースの再生を止める
	alSourceStop(source);
	//ALソースにキューされたバッファを取り出してキューを空に
	empty();
	//ALソースを消去
	alDeleteSources(1, &source);
	check();
	//ALバッファを消去
	alDeleteBuffers(2, buffers);
	check();

	//OGGストリームを開放
	ov_clear(&oggStream);

}

void OGVPlayer::display_info() {

	std::cout << "version         " << vorbisInfo->version << "\n"
			<< "channels        " << vorbisInfo->channels << "\n"
			<< "rate (hz)       " << vorbisInfo->rate << "\n"
			<< "bitrate upper   " << vorbisInfo->bitrate_upper << "\n"
			<< "bitrate nominal " << vorbisInfo->bitrate_nominal << "\n"
			<< "bitrate lower   " << vorbisInfo->bitrate_lower << "\n"
			<< "bitrate window  " << vorbisInfo->bitrate_window << "\n" << "\n"
			<< "vendor " << vorbisComment->vendor << "\n";

	for (int i = 0; i < vorbisComment->comments; i++)
		std::cout << "   " << vorbisComment->user_comments[i] << "\n";

	if (flagRangeLoop) {
		std::cout << "loop enable:true\n" << "loop start samples:" << loopStart
				<< "\n" << "loop end samples:" << loopEnd << "\n";
	} else
		std::cout << "loop enable:false\n";

	std::cout << std::endl;

}

bool OGVPlayer::playback() {

	if (is_Playing())
		return true;

	if (!stream(buffers[0]))
		return false;

	if (!stream(buffers[1]))
		return false;

	alSourceQueueBuffers(source, 2, buffers);
	alSourcePlay(source);

	return true;

}

bool OGVPlayer::is_Playing() {

	ALenum state;

	alGetSourcei(source, AL_SOURCE_STATE, &state);

	return (state == AL_PLAYING);

}

bool OGVPlayer::update() {

	ALint processed_buf;
	bool active = true;
	ALuint buffer;

	alGetSourcei(source, AL_BUFFERS_PROCESSED, &processed_buf);

	while (processed_buf--) {

		alSourceUnqueueBuffers(source, 1, &buffer);
		check();

		if ((active = stream(buffer)) != true)
			break;

		alSourceQueueBuffers(source, 1, &buffer);
	}

	return active;

}

void OGVPlayer::initALObject() {

	if(isALExist) return;

	//ALバッファとALソースを生成。フォーマットは不明なので0を指定。
	alGenBuffers(2, buffers);
	check();
	alGenSources(1, &source);
	check();

	//ソース属性を初期化
	alSource3f(source, AL_POSITION, 0.0, 0.0, 0.0);
	alSource3f(source, AL_VELOCITY, 0.0, 0.0, 0.0);
	alSource3f(source, AL_DIRECTION, 0.0, 0.0, 0.0);
	alSourcef(source, AL_ROLLOFF_FACTOR, 1.0);
	alSourcei(source, AL_SOURCE_RELATIVE, AL_TRUE);

	isALExist = true;

}

bool OGVPlayer::stream(ALuint buffer) {

	char data[BUFFER_SIZE];
	int size = 0;
	int section;
	int result;
	int error_skipped = 0;
	int64_t cullentSample = 0;

	while (size < BUFFER_SIZE) {
		result = ov_read(&oggStream, data + size, BUFFER_SIZE - size, 0, 2, 1,
				&section);

		if (result > 0) {
			size += result;
			error_skipped = 0;
		} else if (result < 0) {
			if (error_skipped < 3)
				error_skipped++;
			else
				throw errorString(result) + "@ stream-main";
		} else
			break;

		if (flagRangeLoop) {
			if ((cullentSample = ov_pcm_tell(&oggStream)) >= loopEnd) {
				size -= (cullentSample - loopEnd) * vorbisInfo->channels * 2;
				std::cout << cullentSample << "," << size << std::endl;
				std::cout << loopStart << std::endl;
				result = ov_pcm_seek(&oggStream, loopStart);
				if (result != 0)
					throw errorString(result) + "@ seek";
			}
		}

	}

	if (size == 0)
		return false;

	alBufferData(buffer, format, data, size, vorbisInfo->rate);
	check();

	return true;

}

void OGVPlayer::empty() {

	int queued_buf;
	ALuint buffer;

	alGetSourcei(source, AL_BUFFERS_QUEUED, &queued_buf);

	while (queued_buf--) {

		alSourceUnqueueBuffers(source, 1, &buffer);
		check();

	}

}

void OGVPlayer::check() {

	std::string content_error;
	ALenum error = alGetError();

	if (error != AL_NO_ERROR) {
		content_error = std::string("OpenAL error was raised : ")
				+ alGetString(error);

		throw content_error;
	}
}

std::string OGVPlayer::errorString(int code) {
	switch (code) {
	case OV_EREAD:
		return std::string("Reading from media error.");
	case OV_ENOTVORBIS:
		return std::string("Not Vorbis data.");
	case OV_EVERSION:
		return std::string("Vorbis version mismatch.");
	case OV_EBADHEADER:
		return std::string("Invalid Vorbis header.");
	case OV_EFAULT:
		return std::string(
				"Internal logic fault (bug or heap/stack corruption.");
	case OV_EINVAL:
		return std::string("Invalid argument error.");
	case OV_ENOSEEK:
		return std::string("Ogg file isn't seekable.");
	case OV_EBADLINK:
		return std::string("Invalid stream section supplied.");

	default:
		return std::string("Unknown Ogg error.");
	}
}
